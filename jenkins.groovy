pipeline{
    agent any
    stages{
        stage("Clean the workspace"){
            steps{
                cleanWs()
            }
        }
        
        stage("git cloning "){
            steps{
                git branch: '${git_branch}', credentialsId: 'gitlabid', url: '${git_url}'
            }
        }
    }
}
